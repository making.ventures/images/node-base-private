FROM registry.gitlab.com/making.ventures/images/node-base

COPY certs/* /usr/local/share/ca-certificates
RUN update-ca-certificates
